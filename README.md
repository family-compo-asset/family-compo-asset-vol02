# Family Compo (F.COMPO) Asset Library
## Volume 02

See [README.md](https://gitlab.com/family-compo-asset/family-compo-asset-vol01/blob/master/README.md) in the [Asset library for manga Family Compo (F.COMPO), volume 01](https://gitlab.com/family-compo-asset/family-compo-asset-vol01).

参见[Asset library for manga Family Compo (F.COMPO), volume 01](https://gitlab.com/family-compo-asset/family-compo-asset-vol01)里的中文说明文档[README-CN.md](https://gitlab.com/family-compo-asset/family-compo-asset-vol01/blob/master/README-CN.md).